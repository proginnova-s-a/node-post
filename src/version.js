#!/usr/bin/env node

const ArgumentParser = require('argparse').ArgumentParser;
const path = require('path');
const fs = require('fs');

const PATH = process.env.PWD|| process.cwd() || __dirname;

const parser = new ArgumentParser({
    version: '0.0.1',
    addHelp: true,
    description: 'Argparse example'
});
parser.addArgument(
    ['-r', '--read'],
    {
        help: 'reads version',
        action: 'storeTrue'
    }
);
parser.addArgument(
    ['-i', '--increase'],
    {
        help: 'increase version',
        choices: ['version', 'minimum', 'patch']
    }
);
parser.addArgument(
    ['-f', '--file'],
    {
        help: 'reads version',
        type: 'string'
    }
);

/** @type {{read:boolean, increase:'version'|'minimun'|'patch', file:string}} */
const args = parser.parseArgs();

if (args.read && args.file) {
    fs.readFile(path.join(PATH, args.file), 'utf8', (err, data) => {
        const version = JSON.parse(data).version;
        console.log(version);
    });
}

if (args.increase && args.file) {
    const filePath = path.join(PATH, args.file);
    fs.readFile(filePath, 'utf8', (err, data) => {
        const json = JSON.parse(data);
        const version = json.version;
        data = data.replace(`"version": "${version}"`, `"version": "${newFunction(version)}"`);
        fs.writeFileSync(filePath, data, 'utf8');
        console.log('done');
    });
}

/**
 * 
 * @param {string} version 
 */
function newFunction(version) {

    /** @type {string[]} */
    const a = version.split('.');

    switch (args.increase) {
        case 'version':
            a[0] = (parseInt(a[0]) + 1).toString();
            a[1] = '0';
            a[2] = '0';
            break;
        case 'minimum':
            a[1] = (parseInt(a[1]) + 1).toString();
            a[2] = '0';
            break;
        case 'patch':
            a[2] = (parseInt(a[2]) + 1).toString();
            break;
    }

    return convertArrayToStr(a);
}

/**
 * 
 * @param {string[]} value 
 */
function convertArrayToStr(value) {
    const a = value.toString();
    return a.replace(/,/g, '.');
}