const ArgumentParser = require('argparse').ArgumentParser;

var parser = new ArgumentParser({
    version: '0.0.1',
    addHelp: true,
    description: 'Argparse example'
});
parser.addArgument(
    ['-u', '--user'],
    {
        help: 'assign user to the credentials',
        required: true
    }
);
parser.addArgument(
    ['-p', '--password'],
    {
        help: 'assign password to the credentials',
        required: true
    }
);
parser.addArgument(
    '--host',
    {
        help: 'assign host for the http request',
        required: true
    }
);
parser.addArgument(
    ['-r', '--registry'],
    {
        help: 'Login credentials for registry',
    }
)
parser.addArgument(
    ['-f', '--file'],
    {
        help: 'docker config file',
        required: true
    }
)
parser.addArgument(
    ['-iv', '--image_version'],
    {
        help: 'Sets specific image version for Docker'
    }
)

parser.addArgument(
    ['--additional_env'],
    {
        help: 'Set aditional arguments'
    }
)

parser.addArgument(
    ['-env','--enviroment'],
    {
        help: 'Sets deploy enviroment'
    }
)

module.exports = parser;