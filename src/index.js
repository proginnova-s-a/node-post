#!/usr/bin/env node

'use strict';

/** 
 * @typedef {Object} Namespace
 * @property {!string} user
 * @property {!string} password
 * @property {!string} host 
 * @property {!string} method 
 * @property {!string} file
 * @property {?string} registry
 * @property {?string} image_version
 * @property {?string} additional_env
 * @property {?string} enviroment
 */


const path = require('path');
const axios = require('axios');
const fs = require('fs');

/** @type {string} */
const PATH = process.env.PWD || process.cwd() || __dirname;

const parser = require('./argparse');

/** @type {Namespace} */
const args = parser.parseArgs();

let registryCredentials = args.registry ? parseRegistryCredentials(args.registry) : undefined;
registryCredentials = registryCredentials ? toBase64(JSON.stringify(registryCredentials)) : undefined;

const filePath = path.join(PATH, args.file);

let json = JSON.parse(fs.readFileSync(filePath, 'utf8'));

const credentials = { user: args.user, password: args.password };
const headers = {
    auth: toBase64(JSON.stringify(credentials))
};

if (registryCredentials)
    headers.registryCredentials = registryCredentials;

if (args.enviroment)
    json = json[args.enviroment];

if (args.image_version)
    json.image = args.image_version;

if (args.additional_env) {
    json.environment = [
        ...json.environment,
        ...args.additional_env.split(/,|;/)
    ]
}

axios.post(args.host, json, { headers })
    .then((res) => {
        console.log(`statusCode: ${res.status}`)
        console.log(res.data)
        process.exit(0);
    })
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });

/**
 * 
 * @param {string} value 
 */
function parseRegistryCredentials(value) {
    /**
     * @type {{user:string,pass:string,host:string}}
     */
    const returnValue = {
        user: undefined,
        pass: undefined,
        host: undefined
    };

    value.split(',').forEach(a => {
        const arr = a.split('=');
        returnValue[arr[0]] = arr[1];
    });



    if (!(returnValue.user && returnValue.pass && returnValue.host)) {
        console.error(returnValue);
        process.exit(1);
    }

    return { user: returnValue.user, pass: returnValue.pass, host: returnValue.host };
}

/**
 * 
 * @param {string} value 
 */
function toBase64(value) {
    return Buffer.from(value).toString('base64');
}